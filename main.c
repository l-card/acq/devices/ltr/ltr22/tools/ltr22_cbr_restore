#include "ltr/include/ltrapi.h"
#include "ltr/include/ltr22api.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <math.h>

static const int range_mV [LTR22_RANGE_CNT] = {1000, 300, 100, 30, 10000, 3000};
static BYTE fw_data[8192];

static BOOL f_coef_valid(float coef) {
    return isfinite(coef);
}
static BOOL f_coef_pair_valid(const TLTR22_ADC_CHANNEL_CALIBRATION *cbr, DWORD range) {
    return  f_coef_valid(cbr->FactoryCalibScale[range]) && f_coef_valid(cbr->FactoryCalibOffset[range])
                                && (cbr->FactoryCalibScale[range] != 0);
}

static INT f_check_ltr22(const char *csn, WORD slot, BOOL *pupdated) {
    INT err = LTR_OK;
    TLTR22 hltr22;
    LTR22_Init(&hltr22);


    err = LTR22_Open(&hltr22, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT, csn, slot);
    if (err != LTR_OK) {
        fprintf(stderr, "Cannot open LTR22 (crate %s, slot %d). Error %d: %s\n", csn, slot, err, LTR22_GetErrorString(err));
    } else {
        DWORD range_idx, freq_idx, ch_idx;
        DWORD bad_cor_ch_cnt = 0;
        DWORD bad_uncor_ch_cnt = 0;

        fprintf(stdout, "LTR22 (crate %s, slot %d, serial %s):\n", csn, slot, hltr22.ModuleInfo.Description.SerialNumber);
        fprintf(stdout, "    Start check coefficients...\n");



        for(range_idx = 0; range_idx < LTR22_RANGE_CNT; ++range_idx) {
            for(freq_idx = 0; freq_idx < LTR22_ADC_FREQ_CNT; ++freq_idx) {
                for(ch_idx = 0; ch_idx < LTR22_CHANNEL_CNT; ++ch_idx) {
                    TLTR22_ADC_CHANNEL_CALIBRATION *c = &hltr22.ADCCalibration[ch_idx][freq_idx];
                    if (!f_coef_pair_valid(c, range_idx)) {
                        BOOL fnd_replace = FALSE;


                        if (freq_idx <= (LTR22_ADC_FREQ_CNT - 6)) {
                            DWORD check_freq;
                            for (check_freq = 0; (check_freq <= (LTR22_ADC_FREQ_CNT - 6)) && !fnd_replace; check_freq++) {
                                const TLTR22_ADC_CHANNEL_CALIBRATION *check_cbr = &hltr22.ADCCalibration[ch_idx][check_freq];
                                if (f_coef_pair_valid(check_cbr, range_idx)) {
                                    c->FactoryCalibScale[range_idx] = check_cbr->FactoryCalibScale[range_idx];
                                    c->FactoryCalibOffset[range_idx] = check_cbr->FactoryCalibOffset[range_idx];
                                    fnd_replace = TRUE;
                                }
                            }
                        }



                        printf("        Ch%d %5dmV %5dHz - %s corruption!\n",
                               ch_idx+1, range_mV[range_idx], LTR22_DISK_FREQ_ARRAY[freq_idx], fnd_replace ? "correctable" : "uncorrectable");
                        if (fnd_replace) {
                            bad_cor_ch_cnt++;
                        } else {
                            bad_uncor_ch_cnt++;
                        }
                    }
                }
            }
        }

        if ((bad_cor_ch_cnt == 0) && (bad_uncor_ch_cnt == 0)) {
            fprintf(stdout, "    No corrupted coefficients were found.\n");
            fflush(stdout);
        } else {
            fprintf(stdout, "    %d corrupted coefficients were found!!! %d correctable, %d uncorrectable\n",
                    bad_cor_ch_cnt, bad_cor_ch_cnt, bad_uncor_ch_cnt);
            if (bad_cor_ch_cnt > 0) {
                fprintf(stdout, "    Start write corrected coefficients...\n");
                fflush(stdout);
                err = LTR22_ReadAVRFirmware(&hltr22, fw_data, sizeof(fw_data), 0, 128);
                if (err == LTR_OK) {
                    err = LTR22_WriteAVRFirmvare(&hltr22, fw_data, sizeof(fw_data), TRUE, TRUE);
                }

                if (err != LTR_OK) {
                    fprintf(stderr, "    Cannot write coefficients. Error %d: %s\n", err, LTR22_GetErrorString(err));
                } else {
                    fprintf(stdout, "    Coefficients were written successfully!\n");
                    fflush(stdout);
                    if (pupdated != NULL)
                        *pupdated = TRUE;
                }
            }

            if (bad_uncor_ch_cnt > 0) {
                err = LTR_ERROR_NOT_IMPLEMENTED;
                fprintf(stderr, "    Cannot correct %d coefficients!\n", bad_uncor_ch_cnt);
            }
        }
    }
    LTR22_Close(&hltr22);
    return err;
}



int main(int argc, char** argv) {
    int err = LTR_OK;
#ifdef _WIN32
    setlocale(LC_ALL, "");
#endif

    /* устанавливаем управляющее соединение с сервером для получения
     * списка крейтов */
    TLTR hltr;
    LTR_Init(&hltr);
    err = LTR_OpenSvcControl(&hltr, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT);
    if (err) {
        fprintf(stderr, "Cannot open connection with ltrd. Error %d: %s!\n",
                err, LTR_GetErrorString(err));
    } else {
        /* получаем список крейтов */
        char crates[LTR_CRATES_MAX][LTR_CRATE_SERIAL_SIZE];
        err = LTR_GetCrates(&hltr, (BYTE*)crates);
        if (err) {
            fprintf(stderr, "Cannot get crate list from ltrd. Error %d: %s!\n",
                    err, LTR_GetErrorString(err));
        }

        if (!err) {
            DWORD modules_ok=0, modules_err = 0, modules_skip = 0;
            WORD i;
            /* проходимся по всем крейтам, но если не было параметра --all,
             * то завершаем после первого найденного модуля */
            for (i=0; i < LTR_CRATES_MAX; i++) {
                if (crates[i][0]!=0) {
                    /* для каждого действительного серийного номера
                       (если явно задан серийный - то только для него)
                       устанавливаем соединение, чтобы получить список модулей */
                    TLTR hcrate;
                    INT cr_err = LTR_OK;

                    LTR_Init(&hcrate);
                    cr_err = LTR_OpenCrate(&hcrate, hltr.saddr, hltr.sport,
                                           LTR_CRATE_IFACE_UNKNOWN, crates[i]);
                    if (cr_err) {
                        fprintf(stderr, "Cannot open crate with serial = %s. Error %d: %s!\n",
                            crates[i], cr_err, LTR_GetErrorString(cr_err));
                    } else {
                        WORD mids[LTR_MODULES_PER_CRATE_MAX];
                        cr_err = LTR_GetCrateModules(&hcrate, mids);
                        if (cr_err) {
                            fprintf(stderr, "Cannot get modules list for crate %s. Error %d: %s!\n",
                                hcrate.csn, cr_err, LTR_GetErrorString(cr_err));
                        } else {
                            INT m;
                            for (m=0; m < LTR_MODULES_PER_CRATE_MAX; m++) {
                                if (mids[m]==LTR_MID_LTR22) {
                                    BOOL updated = FALSE;
                                    int merr = f_check_ltr22(crates[i], LTR_CC_CHNUM_MODULE1  + m, &updated);
                                    if (merr != LTR_OK) {
                                        modules_err++;
                                    } else if (updated) {
                                        modules_ok++;
                                    } else {
                                        modules_skip++;
                                    }
                                }
                            }
                        }
                        LTR_Close(&hcrate);
                    }
                }
            }

            printf("\nResults:\n");
            printf("    Total modules: %d\n    With corrupted coefficients: %d\n    Successfully corrected: %d\n    Error occurred: %d\n",
                   modules_skip+modules_ok+modules_err, modules_ok+modules_err, modules_ok, modules_err);
        }
        LTR_Close(&hltr);
    }
    return err;
}
